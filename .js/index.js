let firstName;
        do {
            firstName = prompt("Введіть Ваше ім'я:");
        } while (!/^[\p{L}\s']+$/u.test(firstName));

let lastName;
        do {
            lastName = prompt("Введіть Ваше прізвище:");
        } while (!/^[\p{L}\s']+$/u.test(lastName));

let birthday;
        do {
            birthday = prompt("Введіть Вашу дату народження у форматі ДД.ММ.РРРР");
        } while (!/^[\d.]+$/.test(birthday));       

function createNewUser() {
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin: function () {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        getAge: function () {
            userbirthday = new Date(this.birthday.slice(6, 10), ((this.birthday.slice(3, 5)) - 1), this.birthday.slice (0, 2));
            now = new Date();
            age = Math.floor((now - userbirthday) / 31557600000);
            return age    
        },
        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6)}`
        }
    }
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());